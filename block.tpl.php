<?php
?>

<?php if (empty($block->subject)){ 
        $class = 'empty-header';
      }
     ?>

<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block block block-<?php print $block->module ?> <?php print $class;?>">
    <?php if (!empty($block->subject)){ ?>
      <h2><?php print $block->subject ?></h2>
    <?php }
          else {
     ?>
     <div class="block_header"></div>
     <?php }
    ?>

  <div class="content"><?php print $block->content ?></div>
</div>
