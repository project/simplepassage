Readme.txt
Created 14th June 2009 by Cignex
www.cignex.com
*********************************

Information
------------
This is a artsy theme.

Please do let me know of any bugs or features you would like to see.

Features
--------
This theme has the following features:

* Site name
* Logo
* Slogan
* Left and right sidebars - in any configuration


Install
-------
Install the theme as you would for any other theme.
